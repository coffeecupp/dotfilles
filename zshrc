zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completion:*' original true
zstyle :compinstall filename '/Users/coffeecupp/.zshrc'

# Completion engines
autoload -Uz compinit
compinit

# History
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Emacs bindings
bindkey -e
export EDITOR="emacsclient --socket-name="server" -c"
alias ee=$EDITOR


# Auto change in dir
setopt AUTO_CD

# Color on osx
export CLICOLOR=1

# Spell check
setopt CORRECT

# Prompt          # GREY        # Slightly more grey # greenish??
export prompt='%F{240}%m%K{000} %F{251}[%~]%K{000} %F{150}-->%K{000} '


# Virtualenv
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
source /usr/local/bin/virtualenvwrapper.sh 
export WORKON_HOME=~/.virtualenvs


alias vim="emacs -nw"

# CHRUBY
source /usr/local/opt/chruby/share/chruby/chruby.sh
source /usr/local/opt/chruby/share/chruby/auto.sh
chruby ruby-2.4.2

# Add home bin to path
path+=("$HOME/bin")
export path

# GPG
export GPG_TTY=$(tty)