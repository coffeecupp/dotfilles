;;; init.el --- This is the first file loaded
;;; Commentary:
;;; A whole bunch of settings all found in the load path
;;; Code:

(package-initialize)


;;; set the load path for easy manipulation
(setq user-init-file (or load-file-name (buffer-file-name))
      user-emacs-directory (file-name-directory user-init-file))

;;; Here is where everything is stored
(add-to-list 'load-path
             (concat user-emacs-directory "/lisp.d/"))

;;; Settings that should be default
(require 'misc-settings)

;;; How emacs should look
(require 'appearance-settings)

(require 'package-settings)

;;; Major mode settings
(require 'mode-settings)

;;; mail settings finally
(require 'mail-settings)

;;; hydra
(require 'hydra-bindings)

;;; init.el ends here
(provide 'init)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (counsel-gtags web-mode virtualenvwrapper use-package robe org-journal oauth2 markdown-mode js2-mode ivy-hydra ggtags flycheck exec-path-from-shell evil-surround evil-mu4e evil-magit evil-leader enh-ruby-mode django-snippets django-mode django-manage counsel company-web company-tern company-quickhelp company-php company-jedi company-inf-ruby chruby ag))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
