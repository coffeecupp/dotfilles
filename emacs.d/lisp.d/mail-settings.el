;;; mail-settings --- read your mail in your text editor
;;; Commentary:
;;; Mu4e for mail is the best!!
;;; Code:
(use-package mu4e
  :load-path "/usr/local/Cellar/mu/0.9.18_1/share/emacs/site-lisp/mu/mu4e"
  :init
  (setq user-mail-address "samuelmcilwain94@gmail.com"
	user-full-name "Sam Mcilwain")
  (use-package evil-mu4e
    :ensure t)
  :config
  ;;; Use mbsync
  (setq mu4e-get-mail-command "mbsync -a"
	;;; Set the maildir
	mu4e-maildir (expand-file-name "~/.maildir/gmail")
	mu4e-sent-folder "/gmailsent"
	mu4e-drafts-folder "/gmaildrafts"
	mu4e-trash-folder "/gmailtrash"
	;;; Enable org html
	org-mu4e-convert-to-html t
	;;; Show images
	mu4e-show-images t
	;;; Show images inline
	mu4e-view-show-images t
	;;; KILL THINGS PLEASE
	message-kill-buffer-on-exit t
	;;; Only show 18
	mu4e-headers-visible-lines 18)
  ;;; Nice little shortcuts
  (setq mu4e-maildir-shortcuts
	'( ("/inbox"               . ?i)
	   ("/gmailsent"   . ?s)
	   ("/gmailtrash"       . ?t)))
  ;;; How to handle sending emails
  (setq message-send-mail-function 'message-send-mail-with-sendmail
	sendmail-program "/usr/local/bin/msmtp"))



;;; mail-settings.el ends here
(provide 'mail-settings)
