;;; hydra-bindings --- coffeecupp
;;; Commentary:
;;; Hydra is good for grouping together keybindings.
;;; Code:


(use-package hydra
  :ensure t
  :init
  (use-package ivy-hydra
    :ensure t)
  :bind (("C-c C-h m" . magit-hydra/body)
	 ("C-c C-h p" . projectile-motions/body)))

(defhydra magit-hydra (:color red :columns 3)
  "
                    magit!
  "
  ("mp" magit-push "Push")
  ("mc" magit-commit "Commit")
  ("md" magit-diff "Diff")
  ("mla" magit-log-all "Log All")
  ("ms" magit-status "Magit Status")
  ("q" nil "Cancel"))

(defhydra projectile-motions (:color pink :columns 4)
  "
                    Project Motions
  "
  ("gu" ggtags-update-tags "Update tags")
  ("gc" ggtags-create-tags "Create tags")
  ("gf" projectile-find-file "Find File")
  ("ga" counsel-ag "Silver Search")
  ("q" nil "Cancel"))

(provide 'hydra-bindings)
;;; hydra-bindings ends here
