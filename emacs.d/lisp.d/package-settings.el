;;; package-settings --- USE PACKAGE ALL THE THINGS
;;; Commentary:
;;; These packages will be loaded with use package
;;; Most of them done through elpa and melpa
;;; Code:
(require 'package)
;;; pretty sure this is just for windows and apparently dos????
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))

(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/")))

(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

;;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;;; Mac doesn't like passing the path unless it lives in the application folder
(use-package exec-path-from-shell
  :ensure t
  :if (memq window-system '(mac ns))
  :init
  (exec-path-from-shell-initialize))


;;; Make emacs even better
(use-package evil
  :ensure t
  :diminish undo-tree-mode
  :bind (
	 ("C-u" . evil-scroll-up)
	 ;;; esc should quit
	 (:map evil-normal-state-map ([escape] . keyboard-quit))
	 (:map evil-visual-state-map ([escape] . keyboard-quit))
	 (:map minibuffer-local-map ([escape] . minibuffer-keyboard-quit))
	 (:map minibuffer-local-ns-map ([escape] . minibuffer-keyboard-quit))
	 (:map minibuffer-local-completion-map ([escape] . minibuffer-keyboard-quit))
	 (:map minibuffer-local-must-match-map ([escape] . minibuffer-keyboard-quit))
	 (:map minibuffer-local-isearch-map ([escape] . minibuffer-keyboard-quit))
	 ;;; Org evil sucks.....
	 (:map evil-normal-state-map
	       (:map org-mode-map
		     ("C->" . org--metashift-right)
		     ("C-<" . org-metashift-left))))
  :init
  ;;; Change the cursor color depending on the mode of evil
  (setq evil-emacs-state-cursor '("red" box)
	evil-normal-state-cursor '("green" box)
	evil-visual-state-cursor '("orange" box)
	evil-insert-state-cursor '("red" bar)
	evil-replace-state-cursor '("red" bar)
	evil-operator-state-cursor '("red" hollow))

  (use-package evil-leader
    :ensure t
    :config
    (global-evil-leader-mode)
    (evil-leader/set-leader ",")
    (evil-leader/set-key
      "e" 'find-file
      "a" 'counsel-git-grep
      "o" 'org-agenda
      "d" 'dired
      "m" 'magit-status
      "f" 'ggtags-find-file
      "g" 'ggtags-find-definition))


  (use-package evil-surround
    :ensure t
    :config
    (global-evil-surround-mode))
  :config
  (evil-mode 1))

(use-package company
  :ensure t
  :bind (:map company-active-map
	      ("C-n" . company-select-next)
	      ("C-p" . company-select-previous))
  :init
  (setq company-tooltip-limit 10
	company-idle-delay 0.2
	company-echo-delay 0
	company-minimum-prefix-length 3
	company-require-match nil
	company-selection-wrap-around t
	company-tooltip-align-annotations t
	company-tooltip-flip-when-above t)
  (use-package company-quickhelp
    :init
    (company-quickhelp-mode 1)))

(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :mode ("/\\.emacs\\.d/snippets/" . snippet-mode)
  :config
  (yas/load-directory (concat user-emacs-directory "/elpa/django-snippets-20131229.811/snippets"))
  :init
  (progn
    (setq yas-verbosity 3)
    (yas-global-mode 1)))

;;; projectile
(use-package projectile
  :ensure t
  :config
  (projectile-global-mode 1)
  (setq projectile-remember-window-configs t
	projectile-completion-system 'ivy))

;;; Better interaction with commands
(use-package counsel
  :ensure t
  :bind (("C-x C-f" . counsel-find-file)
         ("C-h f" . counsel-describe-function)
         ("C-h v" . counsel-describe-variable)
         ("M-y" . counsel-yank-pop))
  :config
  ;;; Ivy also adds some nice things
  (use-package ivy
    :after counsel
    :ensure t
    :init
    (setq ivy-use-virtual-buffers t
	  ivy-count-format ""
	  ivy-use-virtual-buffers t
	  ivy-initial-inputs-alist nil)
    :bind (("C-s" . swiper)
	   ("M-x" . counsel-M-x)
	   ("C-x C-f" . counsel-find-file))
    :config
    (ivy-mode t))
  :init
  (counsel-mode t))

;;; magit for git interaction
(use-package magit
  :ensure t
  :after ivy
  :init
  (use-package evil-magit :ensure t)
  :config
  ;;; use ivy to read things
  (setq magit-completing-read-function 'ivy-completing-read))

(use-package flycheck
  :ensure t
  :init
  ;;; when to check
  (setq flycheck-check-syntax-automatically
	'(save
	  idle-change
	  mode-enabled)))

(use-package jedi-core
  :ensure t
  :init
  (setq jedi:environment-root "Python3")
  :config
  (setq jedi:environment-virtualenv
	(append python-environment-virtualenv
		'("--python" "/usr/local/bin/python3")))

  (use-package company-jedi
    :ensure t
    :after jedi-core company
    :init
    (setq company-jedi-python-bin "/usr/local/bin/python3")
    :config
    (add-to-list 'company-backends 'company-jedi)))

(use-package chruby
  :ensure t
  :init
  (chruby "ruby-2.4.2"))

(use-package org
  :ensure t
  :bind
  (:map evil-normal-state-map
		     ("C->" . org--metashift-right)
		     ("C-<" . org-metashift-left)))
(provide 'package-settings)
;;; package-settings.el ends here
