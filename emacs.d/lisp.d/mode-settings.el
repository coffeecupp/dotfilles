;;; mode-settings.el --- Major mode settings
;;; Commentary:
;;; Some major modes need to be extended
;;; Code:

(add-hook 'prog-mode-hook
          (lambda ()
	    "Universal programming mode settings."
            ;; Insert matching braces
            (electric-pair-mode t)
            ;; also show them
            (show-paren-mode t)
	    ;;; show the clock
	    (display-time-mode t)
	    ;;; Some spell checking
	    (flyspell-prog-mode)
	    ;;; enable auto completion
	    (company-mode t)
	    ;;; give me that error checking
	    (flycheck-mode t)
	    ;; gnu/global
	    (ggtags-mode t)
	    ;;; more advanced line number stuff
	    (setq display-line-number-width 1
		  display-line-numbers 'relative)))

(add-hook 'org-mode-hook
	  (lambda()
	    "Org specific settings."
	    ;;; lets have some auto fill stuff
	    (turn-on-auto-fill)
	    ;;; Sometimes org files get really big
	    (hl-line-mode t)
	    ;;; And actual spell check
	    (flyspell-mode 1)
	    ;;; Latex exporting syntax highlighting
	    (setq org-latex-listings 'minted
		  org-latex-packages-alist '(("" "minted"))
		  org-latex-pdf-process
		  '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
		    "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
	    (org-babel-do-load-languages
	     'org-babel-load-languages
	     '((python . t)))
	    ;;; Org agenda settings
	    (setq org-agenda-files (quote ("~/Google Drive/org/"))
		  ;;; I want to archive tasks from individual files into their own matching file
		  org-archive-location "~/Google Drive/org/archive/%s_archive::"
		  org-default-notes-file "~/Google Drive/org/Notes.org")
	    ;;; This is the process of events
	    (setq org-todo-keywords
		  '((sequence "TODO" "IN-PROGRESS" "WAITING" "|" "DONE" "CANCELLED ")))
	    ;;; Tags
	    (setq org-tag-alist '(("@personal" . ?p)
				  ("@school" . ?s)
				  ("@email" . ?e)
				  ("@phone" . ?h)))
	    (setq org-agenda-ndays 7
		  org-agenda-show-all-dates t
		  org-agenda-skip-deadline-if-done t
		  org-agenda-skip-scheduled-if-done t
		  org-agenda-start-on-weekday nil
		  org-deadline-warning-days 14)))

(add-hook 'python-mode-hook
	  (lambda ()
	    "Python specific mode settings."
	    (setq python-shell-interpreter "ipython")
	    (setq flycheck-checker 'python-flake8)
	    (setq fill-column 90)
	    (jedi:setup)))

;;; django
(add-to-list 'auto-mode-alist '("\\.djhtml$" . django-html-mode))

;; auto open html in web mode
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-hook 'web-mode-hook
	  (lambda ()
	  "Web mode for nicer settings"
	  (setq web-mode-markup-indent-offset 2
		web-mode-css-indent-offset 2
		web-mode-enable-css-colorization t
		web-mode-enable-auto-pairing t)
	  (add-to-list 'company-backends 'company-ac-php-backend)
	  (add-to-list 'company-backends 'company-web-html)))

(add-hook 'css-mode-hook
	  (lambda ()
	    "CSS mode settings"
	    (add-to-list 'company-backends 'company-css)))

(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-hook 'js2-mode-hook
	  (lambda ()
	    "JavaScript"
	    (tern-mode)
	    (add-to-list 'company-backends 'company-tern)
	    ;;; set tabs
	    (setq js2-basic-offset 2)
	    (setq tab-width 2)))

;; which files to use for ruby
(add-to-list 'auto-mode-alist '("Capfile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Rakefile" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rb\\'" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.ru\\'" . ruby-mode))
;; ruby and robe
(add-hook 'ruby-mode-hook
	  (lambda ()
	    "Ruby mode settings and robe mode."
	    (robe-mode)
	    (add-to-list 'company-backends 'company-robe)

	    (setq ruby-deep-arglist t
		  ruby-deep-indent-paren nil
		  c-tab-always-indent nil)
	    ;; Show inf ruby
	    (autoload 'inf-ruby-minor-mode "inf-ruby" "Run an inferior Ruby process" t)
	    (inf-ruby-minor-mode)))

;;; Some markdown support.
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-hook 'markdown-mode-hook
	  (lambda ()
	    "Marrrrkdown"
	    (flyspell-mode 1)
	    (setq tab-width 2
		  fill-column 80
		  word-wrap t)))



(provide 'mode-settings)
;;; mode-settings.el ends here
