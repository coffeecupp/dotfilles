;;; misc-settings.el --- coffeecupp
;;; Commentary:
;;; All the settings that have no other place to live
;;; Code:

;;; Make command meta
(setq mac-option-key-is-meta nil
      mac-command-key-is-meta t
      mac-command-modifier 'meta
      mac-option-modifier 'super)

;;; Don't pause on redisplay
(setq redisplay-dont-pause t)

;;; collect ya shit
(setq gc-cons-threshold (* 100 1024 1024))

;;; Here are a bunch of defaults that I like to be overwritten
(setq
 ;;; make tabs spaces
 indent-tabs-mode nil
 ;;; set c basic off set
 c-basic-offset 4
 ;;; set the width of the tab press
 tab-width 4
 ;;; fill column
 fill-column 80
 ;;; Also tell emacs that I'm using modern english
 sentence-end-with-double-space nil
 ;;; Emacs needs to shut the fuck up
 visual-bell nil
 ring-bell-function 'ignore)

;;; Some Dired settings
(setq dired-recursive-deletes 'always
      dired-recursive-copies 'always
      delete-by-moving-to-trash t
      trash-directory "~/.Trash/")

;;; set the start up screen
(setq inhibit-splash-screen t
      initial-scratch-message "* Welcome To Emacs"
      initial-major-mode 'org-mode)

;;; make yes no y n
(defalias 'yes-or-no-p 'y-or-n-p)

;;; set the backup  directory
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;;; Spell checking
;; find aspell and hunspell automatically
(cond
 ;;; Try find hunspell otherwise use aspell
 ((executable-find "hunspell")
  (setq ispell-program-name "hunspell")
  (setq ispell-local-dictionary "en_AU"))
 ((executable-find "aspell")
  (setq ispell-program-name "ispell")
  (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))))

(provide 'misc-settings)
;;; misc-settings.el ends here
