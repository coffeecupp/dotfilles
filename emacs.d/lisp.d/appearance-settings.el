;;; appearance-settings.el --- how should Emacs look
;;; Commentary:
;;; Since I run Emacs in a gui because I'm not a fruit cake
;;; Most of these settings will relate to that instance
;;; Code:


(add-hook
 'after-init-hook (lambda ()
  ;;; Set everywhere
  ;;; Turn off all the gui stuff
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)

  ;;; Only when emacs is running in the gui
  (when (display-graphic-p)
      ;;; set the theme path
    (add-to-list 'custom-theme-load-path
		 (concat user-emacs-directory "/themes.d/"))

    ;;; Load the actual theme
    (load-theme 'jbeans t)
  
    ;;;  Blink cursor off please!
    (blink-cursor-mode -1)

    ;; set a default font
    (when (member "Hack" (font-family-list))
      (set-face-attribute 'default nil :font "Hack 11"))
  
    ;; initial window
    (setq initial-frame-alist
	  '((width . 180)
	    (height . 59)))

    (setq default-frame-alist
	  '((width . 180)
	    (height . 59))))))

;;; appearance-settings ends here
(provide 'appearance-settings)
