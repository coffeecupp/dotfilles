import os

# Get list of every file in the curent directory
files = os.listdir(os.curdir)
# This is used for later symlinking
dest = os.environ['HOME'] + '.'

# Do it for every file that is in the dir
for file in files:
    # As long as its not a hidden file
    if not file.startswith('.'):
        # Symlink every file to its intended hidden file.
        os.symlink(file, dest + file)
